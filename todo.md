# Sense

Dogfight: Detecting Drones from Drones Videos

SlimYOLOv3: Narrower, Faster and Better for Real-Time UAV Applications



## Radar

Vehicle Detection With Automotive Radar Using Deep Learning on Range-Azimuth-Doppler Tensors

A Novel Semi-Supervised Convolutional Neural Network Method for Synthetic Aperture Radar Image Recognition



# Plan

Teach-Repeat-Replan: A Complete and Robust System for Aggressive Flight in Complex Environments.

FUEL: Fast UAV Exploration Using Incremental Frontier Structure and Hierarchical Planning

Real-time Scalable Dense Surfel Mapping

